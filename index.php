<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Nilai Siswa</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!-- Membuat Container untuk menampung form dan result -->
    <div class="container">
        <h1>Data Nilai Siswa</h1>
        <!-- Membuat Container untuk form -->
        <div class="form">
            <!-- Membuat form -->
            <form action="index.php" method="POST">
                <label for="nama">Nama Siswa :</label><br>
                <input type="text" name="nama"><br>
                <label for="mapel">Mata Pelajaran  :</label><br>
                <select name="mapel" name="mapel">
                    <option value="Matematika">Matematika</option>
                </select>
                <label for="tugas">Nilai Tugas  :</label><br>
                <input type="text" name="tugas"><br>
                <label for="uts">Nilai UTS  :</label><br>
                <input type="text" name="uts"><br>
                <label for="uas">Nilai UAS  :</label><br>
                <input type="text" name="uas"><br>

                <input type="submit" value="Submit">

                <hr>

                <!-- Container untuk result dari input form 
                result ditaruh di container tersendiri agar mudah diberi style -->
                <div class="resultbox">
                    <?php
                        $nama = @$_POST['nama'];
                        $mapel = @$_POST['mapel'];
                        $tugas = @$_POST['tugas'];
                        $uts = @$_POST['uts'];
                        $uas = @$_POST['uas'];

                        // Syntax untuk mencari total nilai
                        $nilai_tugas = $tugas * 0.15;
                        $nilai_uts = $uts * 0.35;
                        $nilai_uas = $uas * 0.5;
                        $jml_nilai = $nilai_tugas + $nilai_uts + $nilai_uas;

                        // perkondisian untuk mencari hasil Grade
                        if($jml_nilai>=90 && $jml_nilai<=100){
                            $grade = "A";
                        }elseif($jml_nilai>70 && $jml_nilai<90){
                            $grade = "B";
                        }elseif($jml_nilai>50 && $jml_nilai<=70){
                            $grade = "C";
                        }elseif($jml_nilai>0 && $jml_nilai<=50){
                            $grade = "D";
                        }else{
                            $grade = null;
                        }

                        // Syntax untuk menampilkan hasil perhitungan dan hasil grade
                        if ($nama){
                            echo "<center><h2>Nilai Akhir Siswa</h2></center>";
                            echo "<strong>Nama Siswa</strong> : {$nama} <br>";
                        }

                        if ($mapel){
                            echo "<strong>Mata Pelajaran</strong>  : {$mapel} <br>";
                        }

                        if ($tugas){
                            echo "<strong>Nilai Tugas</strong>  : {$tugas} <br>";
                        }

                        if ($uts){
                            echo "<strong>Nilai UTS</strong>  : {$uts} <br>";
                        }

                        if ($uas){
                            echo "<strong>Nilai UAS</strong>  : {$uas} <br>";
                        }

                        if ($jml_nilai){
                            echo "<strong>Total Nilai</strong> : {$jml_nilai} <br>";
                        }

                        if ($grade){
                            echo "<strong>Grade</strong> : {$grade} <br>";
                        }

                    ?>
                </div>
            </form>
        </div>
    </div>
</body>
</html>